<?php
class Animal
{
    public $name;
    public $legs = 2;
    public $cold_blood = 'False';

    public function __construct($name)
    {
        $this->name = $name;
    }
}
