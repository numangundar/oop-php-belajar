<?php

require_once('Animal.php');
class Ape extends Animal
{
    public $name;
    public $legs = 2;
    public $cold_blood = 'False';

    public function Yell()
    {
        echo "Auuooooo";
    }
}
